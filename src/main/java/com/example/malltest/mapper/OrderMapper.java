package com.example.malltest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.malltest.entity.Orders;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface OrderMapper extends BaseMapper<Orders> {

}
