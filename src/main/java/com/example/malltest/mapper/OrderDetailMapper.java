package com.example.malltest.mapper;

import com.example.malltest.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface OrderDetailMapper extends BaseMapper<OrderDetail> {

}
