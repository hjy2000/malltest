package com.example.malltest.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.malltest.entity.Orders;
import com.example.malltest.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface OrderService extends IService<Orders> {
    public boolean save(Orders orders, User user,String address,String remark);
}
