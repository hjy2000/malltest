package com.example.malltest.service.impl;

import com.example.malltest.entity.UserAddress;
import com.example.malltest.mapper.UserAddressMapper;
import com.example.malltest.service.UserAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {

}
