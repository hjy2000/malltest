package com.example.malltest.service;

import com.example.malltest.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface OrderDetailService extends IService<OrderDetail> {

}
