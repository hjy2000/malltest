package com.example.malltest.service;

import com.example.malltest.entity.Cart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.malltest.vo.CartVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface CartService extends IService<Cart> {
    public List<CartVO> findAllCartVOByUserId(Integer id);
}
