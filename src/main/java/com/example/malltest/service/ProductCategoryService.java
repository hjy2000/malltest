package com.example.malltest.service;

import com.example.malltest.entity.ProductCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.malltest.vo.ProductCategoryVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HJY
 * @since 2020-12-15
 */
public interface ProductCategoryService extends IService<ProductCategory> {
    public List<ProductCategoryVO> getAllProductCategoryVO();
}
